import './style.css';

const ripple = (evt) => {
  const time = 600;
  const el = evt.currentTarget;

  const circle = document.createElement('div');
  const diameter = Math.max(el.clientWidth, el.clientHeight);
  const radius = diameter / 2;

  circle.classList.add('el-ripple');
  circle.style.width = circle.style.height = `${diameter}px`;

  circle.style.left =
  `${evt.clientX - (el.getBoundingClientRect().left + radius)}px`;
  circle.style.top =
  `${evt.clientY - (el.getBoundingClientRect().top + radius)}px`;
  circle.style.animationDuration = `0.${time}s`;


  el.appendChild(circle);

  const removeEffect = () => {
    setTimeout(() => {
      el.removeChild(circle);
    }, time);

    evt.target.removeEventListener('mouseup', removeEffect);
    evt.target.removeEventListener('mouseleave', removeEffect);
  };

  evt.target.addEventListener('mouseup', removeEffect);
  evt.target.addEventListener('mouseleave', removeEffect);
};

export default ripple;
