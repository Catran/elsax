const generateId = (name) => {
  return name + '-' + Math.random().toString(36).substring(7);
};

const getColorStyle = (type) => {
  if (!!type.danger) {
    return 'danger';
  } else if (!!type.success) {
    return 'success';
  } else if (!!type.warning) {
    return 'warning';
  } else if (!!type.primary) {
    return 'primary';
  } else {
    return null;
  }
};

export {
  generateId,
  getColorStyle,
};
