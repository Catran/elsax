import './style/main.css';
import * as components from './components';
export * from './components';


const install = (app, options) => {
  Object.values(components).forEach((component) => {
    app.use(component);
  });
};

export default install;
