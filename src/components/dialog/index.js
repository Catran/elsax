import './style.css';
import component from './dialog';

component.install = (app) => {
  app.component('ElDialog', component);
};

export default component;
