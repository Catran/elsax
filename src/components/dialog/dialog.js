import {h, Transition, Teleport} from 'vue';

export default {
  name: 'ElAlert',

  inheritAttrs: false,

  props: {
    modelValue: {type: Boolean, required: true},

    label: {type: String, required: false, default: null},

    block: {type: Boolean, required: false, default: false},

    full: {type: Boolean, required: false, default: false},
  },

  data() {
    return {
      isShowDialog: false,
    };
  },

  watch: {
    modelValue() {
      if (this.modelValue === true) {
        setTimeout(() => this.isShowDialog = true, 0);
      }
    },
  },

  render() {
    return this.renderDialogTransition();
  },

  methods: {
    close() {
      this.isShowDialog = false;
      setTimeout(() => {
        this.$emit('update:modelValue', false);
        this.$emit('dialog-close');
      }, 0);
    },

    renderTitle() {
      const options = {
        class: [
          'el-dialog__title',
        ],
      };

      const slots = [
        this.label,
      ];

      return h('div', options, slots);
    },

    renderClose() {
      const options = {
        class: [
          'el-dialog__close',
        ],
        onclick: this.close,
      };

      const slots = [
        h('span', {class: 'el-dialog__close-line'}),
        h('span', {class: 'el-dialog__close-line'}),
      ];

      return h('div', options, slots);
    },

    renderDialog() {
      const options = {
        class: [
          'el-element-normalize',
          'el-dialog',
          {'el-dialog--open': this.isShowDialog},
          {'el-dialog--block': this.block},
          {'el-dialog--full': this.full},
          this.$attrs.class,
        ],
      };

      const slots = [
        this.renderTitle(),
        this.renderClose(),
        this.$slots.default && this.$slots.default(),
      ];

      return h('div', options, slots);
    },


    renderDialogWrapper() {
      const options = {
        class: [
          'el-dialog__wrapper',
        ],
        onclick: (evt) => {
          if (evt.target === evt.currentTarget) {
            this.close();
          }
        },
      };

      const slots = [
        this.renderDialog(),
      ];

      return h('div', options, slots);
    },

    renderDialogTransition() {
      const options = {
        name: 'el-dialog-fade',
      };

      const slots = () => this.modelValue ? this.renderDialogWrapper() : null;

      const tr = h(Transition, options, {default: slots});

      return h(Teleport, {to: 'body'}, tr);
    },
  },
};
