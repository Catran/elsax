import mount from '../../utils/mount';
import loading from './loading';

const Api = (app) => {
  const {vNode} = mount(loading, {
    props: {
      isShow: false,
    },
    app: app,
  });

  document.body.appendChild(vNode.el);

  return {
    show(title = '') {
      vNode.component.props.isShow = true;
      vNode.component.props.title = title;
    },

    hide() {
      vNode.component.props.isShow = false;
    },
  };
};

export default Api;
