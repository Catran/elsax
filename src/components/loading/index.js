import './style.css';
import component from './api';

component.install = (app) => {
  if (!app.config.globalProperties.$elsax) {
    app.config.globalProperties.$elsax = {};
  }

  app.config.globalProperties.$elsax.loading = component(app);
};

export default component;
