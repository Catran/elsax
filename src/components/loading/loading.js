import {h, Transition} from 'vue';

export default {
  name: 'ElLoading',

  props: {
    isShow: {type: Boolean, required: false, default: false},

    title: {type: String, required: false, default: 'Loading'},
  },

  render() {
    return this.renderTransitionWrapper();
  },

  methods: {
    renderTitle() {
      const options = {
        class: ['el-loading__title'],
      };

      const slots = [
        this.title,
      ];

      return h('div', options, slots);
    },

    renderAnimation() {
      const options = {
        class: ['el-loading__animation'],
      };

      const slots = [];

      return h('div', options, slots);
    },

    renderAnimationContainer() {
      const options = {
        class: ['el-loading__container'],
      };

      const slots = [
        this.renderAnimation(),
        this.title && this.renderTitle(),
      ];

      return h('div', options, slots);
    },

    renderLoading() {
      const options = {
        class: [
          'el-loading',
        ],
      };

      const slots = [
        this.renderAnimationContainer(),
      ];

      return h('div', options, slots);
    },

    renderTransitionWrapper() {
      const options = {
        name: 'el-loading-fade',
      };

      const slots = this.$props.isShow ? this.renderLoading() : null;

      return h(Transition, options, {
        default() {
          return slots;
        },
      });
    },
  },
};
