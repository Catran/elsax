import mount from '../../utils/mount';
import notifys from './notify';

const Api = (app) => {
  const {vNode} = mount(notifys, {
    props: {
      notifys: [],
    },
    app: app,
  });

  document.body.appendChild(vNode.el);

  return {
    show(title, text, options = {} ) {
      vNode.component.ctx.addMessage(title, text, options);
    },

    danger(title, text, options = {}) {
      Object.assign(options, {type: 'danger'});
      this.show(title, text, options);
    },

    success(title, text, options = {}) {
      Object.assign(options, {type: 'success'});
      this.show(title, text, options);
    },

    warning(title, text, options = {}) {
      Object.assign(options, {type: 'warning'});
      this.show(title, text, options);
    },
  };
};

export default Api;
