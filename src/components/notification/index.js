import './style.css';
import component from './api';

component.install = (app) => {
  if (!app.config.globalProperties.$elsax) {
    app.config.globalProperties.$elsax = {};
  }

  app.config.globalProperties.$elsax.notify = component(app);
};

export default component;
