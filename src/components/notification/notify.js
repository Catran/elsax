import {h, TransitionGroup} from 'vue';
import message from './message';
import {generateId} from '../../utils';

export default {
  name: 'ElNotify',

  props: {
    duration: {type: Number, required: false, default: 5000},
  },

  data() {
    return {
      notifys: [],
    };
  },

  render() {
    return this.randerNotifyWrapper();
  },

  methods: {
    isType(value, type) {
      return value === type;
    },

    clearMessage(id) {
      const index = this.notifys.findIndex((item) => item.id === id);
      this.notifys.splice(index, 1);
    },

    getNotifys() {
      return this.notifys.map(
          (item) => {
            return h(message, {
              id: item.id,
              label: item.title,
              duration: item.options.duration ?? this.duration,
              closable: !!item.options.closable,
              warning: this.isType(item.options.type, 'warning'),
              success: this.isType(item.options.type, 'success'),
              danger: this.isType(item.options.type, 'danger'),
              text: item.text,
              key: item.id,
              onMessageClose: (id) => {
                this.clearMessage(id);
              },
            });
          },
      ).reverse();
    },

    randerNotifyWrapper() {
      const options = {
        name: 'el-notify-fade',
      };

      return h('div', {class: 'el-notify'},
          h(TransitionGroup, options, {
            default: this.getNotifys,
          }),
      );
    },

    addMessage(title, text, options) {
      const uid = generateId('#notify');
      this.notifys.push({id: uid, title, text, options});
    },
  },
};
