import {h} from '@vue/runtime-core';

export default {
  name: 'ElMessage',

  props: {
    id: {type: [String, Number], required: true},

    success: {type: Boolean, required: false, default: false},

    danger: {type: Boolean, required: false, default: false},

    warning: {type: Boolean, required: false, default: false},

    label: {type: String, required: false, default: null},

    text: {type: String, required: false, default: null},

    closable: {type: Boolean, required: false, default: false},

    duration: {type: Number, required: false, default: 5000},
  },

  emits: ['message-close'],

  data() {
    return {
      progress: 1,
      elProggerss: null,
    };
  },

  render() {
    return this.renderMessage();
  },

  methods: {
    close() {
      this.$emit('message-close', this.id);
    },

    renderTitle() {
      const options = {
        class: [
          'el-notify-message__title',
        ],
      };

      return h('div', options, this.label);
    },

    renderText() {
      const options = {
        class: [
          'el-notify-message__text',
        ],
      };

      return h('div', options, this.text);
    },

    renderClose() {
      const options = {
        class: [
          'el-notify-message__close',
        ],
        onclick: this.close,
      };

      const slots = [
        h('span', {class: 'el-notify-message__close-line'}),
        h('span', {class: 'el-notify-message__close-line'}),
      ];

      return h('div', options, slots);
    },

    renderProggers() {
      const options = {
        class: [
          'el-notify-message__progress',
        ],
      };

      this.elProggerss = h('div', options);
      return this.elProggerss;
    },

    renderMessage() {
      const options = {
        class: [
          'el-notify-message',
          {
            'el-notify-message--primary':
            !this.danger && !this.success && !this.warning,
          },
          {'el-notify-message--danger': !!this.danger},
          {'el-notify-message--success': !!this.success},
          {'el-notify-message--warning': !!this.warning},
        ],
      };

      const slots = [
        this.label && this.renderTitle(),
        this.text && this.renderText(),
        (!!this.closable || !this.duration) && this.renderClose(),
        !!this.duration && this.renderProggers(),
      ];

      return h('div', options, slots);
    },
  },

  mounted() {
    if (this.duration > 0) {
      const interval = setInterval(() => {
        this.elProggerss.el.style.width = this.progress + '%';
        this.progress = this.progress + 1;
      }, this.duration / 100);

      setTimeout(() => {
        clearInterval(interval);
        this.close();
      }, this.duration);
    }
  },
};
