import {h} from '@vue/runtime-core';

export default {
  name: 'ElCard',

  inheritAttrs: false,

  props: {
  },

  render() {
    return this.renderCard();
  },

  methods: {
    renderTitle() {
      const options = {
        class: [
          'el-card__title',
        ],
      };

      const slots = [
        this.$slots.title(),
      ];

      return h('div', options, slots);
    },

    renderBody() {
      const options = {
        class:
        [
          'el-card__body',
          {'el-card__body--only': !this.$slots.title && !this.$slots.footer},
        ],
      };

      const slots = [
        this.$slots.default(),
      ];

      return h('div', options, slots);
    },

    renderFooter() {
      const options = {
        class: [
          'el-card__footer',
        ],
      };

      const slots = [
        this.$slots.footer(),
      ];

      return h('div', options, slots);
    },

    renderCard() {
      const options = {
        class: [
          'el-element-normalize',
          'el-card',
          this.$attrs.class,
        ],
      };

      const slots = [
        this.$slots.title && this.renderTitle(),
        this.$slots.default && this.renderBody(),
        this.$slots.footer && this.renderFooter(),
      ];

      return h('div', options, slots);
    },
  },
};
