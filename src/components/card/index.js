import './style.css';
import component from './card';

component.install = (app) => {
  app.component('ElCard', component);
};

export default component;
