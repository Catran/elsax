import './style.css';
import select from './select';
import options from './options';

// TODO: fix this component show.
select.install = (app) => {
  app.component('ElSelect', select);
  app.component('ElOption', options);
};

export default select;
