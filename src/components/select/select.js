import {h} from '@vue/runtime-core';
import {getColorStyle} from '../../utils';

export default {
  name: 'ElSelect',

  inheritAttrs: false,

  props: {
    primary: {type: Boolean, required: false, default: false},

    success: {type: Boolean, required: false, default: false},

    danger: {type: Boolean, required: false, default: false},

    warning: {type: Boolean, required: false, default: false},

    label: {type: String, required: false, default: ''},

    modelValue: {type: [String, Number], required: true},

    placeholder: {type: String, required: false, default: 'Select some value'},

    message: {type: String, required: false, default: ''},

    disabled: {type: Boolean, required: false, default: false},
  },

  data() {
    return {
      open: false,
      title: this.placeholder,
    };
  },

  computed: {
    colorStyle() {
      const color = getColorStyle({
        danger: this.danger,
        success: this.success,
        warning: this.warning,
        primary: this.primary,
      });
      return color ? 'el-select--' + color : '';
    },
  },

  render() {
    return this.renderSelectWrapper();
  },

  methods: {
    change(value, title) {
      title = title.trim();
      this.title = title;
      this.$emit('update:modelValue', value);
    },

    close(e) {
      if (!(this.$el == e.target || this.$el.contains(e.target))) {
        this.open = false;
      }
    },

    renderLabel() {
      const options = {
        for: this.id,
        class: 'el-select__label',
      };

      return h('label', options, this.label);
    },

    renderInput() {
      const options = {
        readonly: 'readonly',
        class: [
          'el-select__input',
        ],
        value: this.title,
        disabled: this.disabled,
      };

      return h('input', options);
    },

    renderArrow() {
      const options = {
        class: [
          'el-select__arrow',
          {'el-select__arrow--open': this.open},
        ],
      };

      return h('div', options);
    },

    renderUnion() {
      const options = {
        class: [
          'el-select__union',
        ],
      };

      const slots = [
        this.renderInput(),
        this.renderArrow(),
      ];

      return h('div', options, slots);
    },

    renderContent() {
      const options = {
        class: [
          'el-select__container',
          {'el-select__container--open': this.open},
        ],
      };

      const slots = [
        this.$slots.default(),
      ];

      return h('div', options, slots);
    },

    renderMessage() {
      const options = {
        class: [
          'el-select__message',
          this.colorStyle,
        ],
      };

      return h('div', options, this.message);
    },

    renderSelectWrapper() {
      const options = {
        class: [
          'el-select__wrapper',
        ],
      };

      const slots = [
        this.renderSelect(),
        this.renderMessage(),
      ];

      return h('div', options, slots);
    },

    renderSelect() {
      const options = {
        class: [
          'el-element-normalize',
          'el-select',
          {'el-select--open': this.open},
          this.colorStyle,
          {'el-select--disabled': !!this.disabled},
        ],
        onClick: () => {
          this.open = !this.open;
        },
      };

      const slots = [
        this.renderLabel(),
        this.renderUnion(),
        this.renderContent(),
      ];

      return h('div', options, slots);
    },
  },

  mounted() {
    document.addEventListener('click', this.close);
  },

  unmounted() {
    document.removeEventListener('click', this.close);
  },
};
