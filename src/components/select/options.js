import {h} from '@vue/runtime-core';

export default {
  name: 'ElOption',

  props: {
    value: {type: [String, Number], required: true},

    disabled: {type: Boolean, required: false, default: null},
  },

  computed: {
    isActive() {
      if (+this.value !== NaN) {
        return this.$parent.modelValue == this.value;
      }
      return this.$parent.modelValue === this.value;
    },

    label() {
      return this.$slots.default()[0].children.trim();
    },
  },

  render() {
    return this.renderOption();
  },

  mounted() {
    if (this.isActive) {
      this.$parent.change(this.value, this.label);
    }
  },

  methods: {
    renderOption() {
      const options = {
        ...this.$attrs,
        class: [
          'el-option',
          {'el-option--disabled': !!this.disabled},
          {'el-option--active': !!this.isActive},
        ],
        onClick: () =>
          this.$parent.change(this.value, this.label),
      };

      const slots = [
        this.$slots.default(),
      ];

      return h('div', options, slots);
    },
  },
};
