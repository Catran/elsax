import {h} from '@vue/runtime-core';

export default {
  name: 'ElTd',

  render() {
    return this.renderTd();
  },

  methods: {
    renderTd() {
      const options = {
        ...this.$attrs,
        class: [
          'el-table__td',
        ],
      };

      const slots = [
        this.$slots.default && this.$slots.default(),
      ];

      return h('td', options, slots);
    },
  },
};
