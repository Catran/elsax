import {h} from '@vue/runtime-core';

export default {
  name: 'ElTr',

  render() {
    return this.renderTr();
  },

  methods: {
    renderTr() {
      const options = {
        ...this.$attrs,
        class: [
          'el-table__tr',
        ],
      };

      const slots = [
        this.$slots.default && this.$slots.default(),
      ];

      return h('tr', options, slots);
    },
  },
};
