import {h} from '@vue/runtime-core';

export default {
  name: 'ElTh',

  render() {
    return this.renderTh();
  },

  methods: {
    renderTh() {
      const options = {
        ...this.$attrs,
        class: [
          'el-table__th',
        ],
      };

      const slots = [
        this.$slots.default && this.$slots.default(),
      ];

      return h('th', options, slots);
    },
  },
};
