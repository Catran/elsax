import './style.css';
import table from './table';
import tr from './tr';
import th from './th';
import td from './td';

table.install = (app) => {
  app.component('ElTable', table);
  app.component('ElTr', tr);
  app.component('ElTh', th);
  app.component('ElTd', td);
};

export default table;
