import {h} from '@vue/runtime-core';

export default {
  name: 'ElTable',

  props: {
    selectable: {type: Boolean, required: false, default: false},

    striped: {type: Boolean, required: false, default: false},
  },

  render() {
    return this.renderTableWrapper();
  },

  methods: {
    renderHeader() {
      const options = {
        class: [
          'el-table__header',
        ],
      };

      const slots = [
        this.$slots.header(),
      ];

      return h('thead', options, slots);
    },

    renderFooter() {
      const options = {
        class: [
          'el-table__footer',
        ],
      };

      const slots = [
        this.$slots.footer(),
      ];

      return h('div', options, slots);
    },

    renderTable() {
      const options = {
        ...this.$attrs,
        class: [
          'el-element-normalize',
          'el-table',
          {'el-table--selectable': !!this.selectable},
          {'el-table--striped': !!this.striped},
        ],
      };

      const slots = [
        this.$slots.header && this.renderHeader(),
        this.$slots.body && this.$slots.body(),
      ];

      return h('table', options, slots);
    },

    renderTableWrapper() {
      const options = {
        ...this.$attrs,
        class: [
          'el-table__wrapper',
        ],
      };

      const slots = [
        this.renderTable(),
        this.$slots.footer && this.renderFooter(),
      ];

      return h('div', options, slots);
    },
  },
};
