import {h} from '@vue/runtime-core';
import {Transition} from 'vue';
import {getColorStyle} from '../../utils';

export default {
  name: 'ElAlert',

  inheritAttrs: false,

  props: {
    success: {type: Boolean, required: false, default: false},

    danger: {type: Boolean, required: false, default: false},

    warning: {type: Boolean, required: false, default: false},

    label: {type: String, required: false, default: null},

    closable: {type: Boolean, required: false, default: false},

    showSeconds: {type: Number, required: false, default: null},
  },

  data() {
    return {
      isClose: false,
      progress: 1,
      elProggerss: null,
    };
  },

  computed: {
    colorStyle() {
      const color = getColorStyle({
        danger: this.danger,
        success: this.success,
        warning: this.warning,
      });
      return 'el-alert--' + (color ? color : 'primary');
    },
  },

  render() {
    return h(Transition, {
      name: 'el-alert-fade',
      onLeave(element) {
        const height = getComputedStyle(element).height;
        element.style.height = height;
        getComputedStyle(element).height;
        requestAnimationFrame(() => {
          element.style.height = 0;
        });
      },
    }, !this.isClose ? this.renderAlert : null);
  },

  methods: {
    close() {
      this.isClose = true;
      this.$emit('alert-close');
    },

    renderTitle() {
      const options = {
        class: [
          'el-alert__title',
        ],
      };

      const slots = [
        this.label,
      ];

      return h('div', options, slots);
    },

    renderClose() {
      const options = {
        class: [
          'el-alert__close',
        ],
        onclick: this.close,
      };

      const slots = [
        h('span', {class: 'el-alert__close-line'}),
        h('span', {class: 'el-alert__close-line'}),
      ];

      return h('div', options, slots);
    },

    renderBody() {
      const options = {
        class: [
          'el-alert__body',
        ],
      };

      const slots = [
        this.$slots.default && this.$slots.default(),
      ];

      return h('div', options, slots);
    },

    renderFooter() {
      const options = {
        class: [
          'el-alert__footer',
        ],
      };

      const slots = [
        this.$slots.footer && this.$slots.footer(),
      ];

      return h('div', options, slots);
    },

    renderProggers() {
      const options = {
        class: [
          'el-alert__progress',
        ],
      };

      this.elProggerss = h('div', options);
      return this.elProggerss;
    },

    renderAlert() {
      const options = {
        class: [
          'el-element-normalize',
          'el-alert',
          this.colorStyle,
          {'el-alert--close': !!this.isClose},
        ],
      };

      const slots = [
        this.label && this.renderTitle(),
        this.closable && this.renderClose(),
        this.renderBody(),
        this.$slots.footer && this.renderFooter(),
        this.showSeconds && this.renderProggers(),
      ];

      return h('div', options, slots);
    },
  },

  mounted() {
    if (this.showSeconds > 0) {
      const interval = setInterval(() => {
        this.elProggerss.el.style.width = this.progress + '%';
        this.progress = this.progress + 1;
      }, this.showSeconds * 10);
      setTimeout(() => {
        clearInterval(interval);
        this.close();
      }, this.showSeconds * 1000);
    }
  },
};
