import './style.css';
import component from './alert';

component.install = (app) => {
  app.component('ElAlert', component);
};

export default component;
