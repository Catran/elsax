import './style.css';
import component from './checkbox';

component.install = (app) => {
  app.component('ElCheckbox', component);
};

export default component;
