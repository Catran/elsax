import {h} from 'vue';
import {generateId, getColorStyle} from '../../utils';

export default {
  name: 'ElCheckbox',

  inheritAttrs: false,

  props: {
    primary: {type: Boolean, required: false, default: false},

    success: {type: Boolean, required: false, default: false},

    danger: {type: Boolean, required: false, default: false},

    warning: {type: Boolean, required: false, default: false},

    modelValue: {type: Boolean, required: true},

    label: {type: String, required: false, default: ''},

    message: {type: String, required: false, default: ''},

    reverse: {type: Boolean, required: false, default: false},

    disabled: {type: Boolean, required: false, default: false},
  },

  data() {
    return {
      id: this.$attrs.id || generateId('el-checkbox'),
    };
  },

  computed: {
    style() {
      const color = getColorStyle({
        danger: this.danger,
        success: this.success,
        warning: this.warning,
        primary: this.primary,
      });
      return color || 'default';
    },
  },

  render() {
    return this.renderCheckbox();
  },

  methods: {
    renderInput() {
      const options = {
        ...this.$attrs,
        type: 'checkbox',
        checked: this.modelValue,
        class: 'el-checkbox__input',
        onChange: (evt) => this.$emit('update:modelValue', evt.target.checked),
        id: this.id,
        disabled: this.disabled,
      };

      return h('input', options);
    },

    renderCheckMask() {
      const check = h('div', {class: ['el-checkbox__mask-check']});

      const options = {
        class: [
          'el-checkbox__mask',
          {'el-checkbox__mask--checked': this.modelValue},
        ],
      };

      return h('div', options, check);
    },

    renderCheck() {
      const options = {
        class: 'el-checkbox__mask-content',
      };

      const slots = [
        this.renderInput(),
        this.renderCheckMask(),
      ];

      return h('div', options, slots);
    },

    renderLabel() {
      const options = {
        for: this.id,
        class: 'el-checkbox__label',
      };

      return h('label', options, this.label);
    },

    renderUnion() {
      const options = {
        class: [
          'el-checkbox__union',
          {'el-checkbox__union--reverse': !!this.reverse},
        ],
      };

      const slots = [
        this.renderCheck(),
        this.label && this.renderLabel(),
      ];

      return h('div', options, slots);
    },

    renderMessage() {
      const options = {
        class: 'el-checkbox__message',
      };

      return h('div', options, this.message);
    },

    renderCheckbox() {
      const options = {
        class: [
          'el-element-normalize',
          'el-checkbox',
          'el-checkbox--' + this.style,
          {'el-checkbox--disabled': !!this.disabled},
        ],
      };

      const slots = [
        this.renderUnion(),
        this.message && this.renderMessage(),
      ];

      return h('div', options, slots);
    },
  },
};
