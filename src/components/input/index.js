import './style.css';
import component from './input';

component.install = (app) => {
  app.component('ElInput', component);
};

export default component;
