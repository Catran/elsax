import {h, Transition} from 'vue';
import {generateId, getColorStyle} from '../../utils';

export default {
  name: 'ElInput',

  inheritAttrs: false,

  props: {
    primary: {type: Boolean, required: false, default: false},

    success: {type: Boolean, required: false, default: false},

    danger: {type: Boolean, required: false, default: false},

    warning: {type: Boolean, required: false, default: false},

    modelValue: {type: [String, Number], required: true},

    label: {type: String, required: false, default: ''},

    message: {type: String, required: false, default: ''},

    block: {type: Boolean, required: false, default: false},

    disabled: {type: Boolean, required: false, default: false},
  },

  data() {
    return {
      id: this.$attrs.id || generateId('el-input'),
    };
  },

  computed: {
    style() {
      const color = getColorStyle({
        danger: this.danger,
        success: this.success,
        warning: this.warning,
        primary: this.primary,
      });
      return color || 'default';
    },
  },

  render() {
    return this.renderInput();
  },

  methods: {
    renderLabel() {
      const options = {
        for: this.id,
        class: 'el-input__label',
      };

      return h('label', options, this.label);
    },

    renderControl() {
      const options = {
        class: 'el-input__input',
        ...this.$attrs,
        value: this.modelValue,
        onInput: (evt) => {
          this.$emit('update:modelValue', evt.target.value);
          this.$attrs.onInput();
        },
        placeholder: null,
        id: this.id,
        type: this.$attrs.type || 'text',
        disabled: this.disabled,
      };

      return h('input', options, null);
    },

    renderMessage() {
      const options = {
        class: 'el-input__message',
      };

      const message = () => {
        return this.message ? h('div', options, this.message) : null;
      };

      const tOptions = {
        name: 'el-input-message-fade',

        onEnter(element) {
          const width = getComputedStyle(element).width;

          element.style.width = width;
          element.style.position = 'absolute';
          element.style.visibility = 'hidden';
          element.style.height = 'auto';

          const height = getComputedStyle(element).height;

          element.style.width = null;
          element.style.position = null;
          element.style.visibility = null;
          element.style.height = 0;

          getComputedStyle(element).height;
          requestAnimationFrame(() => {
            element.style.height = height;
          });
        },

        onAfterEnter(element) {
          element.style.height = 'auto';
        },

        onLeave(element) {
          const height = getComputedStyle(element).height;
          element.style.height = height;
          getComputedStyle(element).height;
          requestAnimationFrame(() => {
            element.style.height = 0;
          });
        },
      };

      return h(Transition, tOptions, {default: message});
    },

    renderInput() {
      const options = {
        class: [
          'el-element-normalize',
          'el-input',
          'el-input--' + this.style,
          {'el-input--block': !!this.block},
          {'el-input--disabled': !!this.disabled},
        ],
      };

      const slost = [
        this.renderLabel(),
        this.renderControl(),
        this.renderMessage(),
      ];

      return h('div', options, slost);
    },
  },
};
