import {h} from '@vue/runtime-core';
import ripple from '../../utils/ripple';
import {getColorStyle} from '../../utils';

export default {
  name: 'ElButton',

  props: {
    success: {type: Boolean, required: false, default: false},

    danger: {type: Boolean, required: false, default: false},

    warning: {type: Boolean, required: false, default: false},
  },

  computed: {
    style() {
      const color = getColorStyle({
        danger: this.danger,
        success: this.success,
        warning: this.warning,
      });
      return color ? color : 'primary';
    },
  },

  render() {
    return this.$slots.default && this.renderButton();
  },

  methods: {
    renderContent() {
      const options = {
        class: 'el-button__content',
      };

      const slots = [
        this.$slots.default(),
      ];

      return h('div', options, slots);
    },

    renderButton() {
      const options = {
        ...this.$attrs,
        class: [
          'el-element-normalize',
          'el-button',
          'el-button--' + this.style,
        ],
        onMousedown: (e) => ripple(e),
      };

      const slots = [
        this.renderContent(),
      ];

      return h('button', options, slots);
    },
  },
};
