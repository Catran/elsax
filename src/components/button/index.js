import './style.css';
import component from './button';

component.install = (app) => {
  app.component('ElButton', component);
};

export default component;
