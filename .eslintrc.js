module.exports = {
  env: {
    'browser': true,
    'es2021': true,
  },
  extends: [
    'plugin:vue/strongly-recommended',
    'google',
  ],
  parserOptions: {
    'ecmaVersion': 12,
    'sourceType': 'module',
  },
  plugins: [
    'vue',
  ],
  rules: {
    'vue/no-multiple-template-root': 0,
  },
};


